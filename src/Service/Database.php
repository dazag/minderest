<?php


namespace Minderest\Service;


use Closure;
use PDO;
use PDOException;

abstract class Database
{

    /**
     * @var PDO
     */
    private static $db;

    /**
     * @param \Closure $query
     *
     * @return mixed|bool
     */
    public static function transaction(Closure $query, $throwsException = false)
    {
        $db = self::connect();
        $result = false;

        try {
            $db->beginTransaction();
            $result = $query($db);
            $db->commit();
        } catch (PDOException $exception) {
            // Undo changes
            $db->rollBack();
            if ($throwsException) {
                throw $exception;
            } else {
                echo $exception->getMessage() . "\n";
            }
        }
        return $result;
    }

    /**
     * @return PDO
     */
    public static function connect(): PDO
    {

        // cached value
        if (isset(self::$db)) {
            return self::$db;
        }

        // @todo add variables as environment vars
        $user = 'david';
        $pass = 'test';
        $host = 'minderest_mysql';
        $db = 'minderest';
        $charset = 'utf8mb4';
        $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
        $options = [
          PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
          PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
          PDO::ATTR_EMULATE_PREPARES => false,
        ];

        try {
            self::$db = new PDO($dsn, $user, $pass, $options);
        } catch (PDOException $e) {
            throw new PDOException($e->getMessage(), (int)$e->getCode());
        }

        return self::$db;
    }
}