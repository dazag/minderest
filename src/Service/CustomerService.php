<?php

namespace Minderest\Service;

use Minderest\Model\Customer;
use PDO;

class CustomerService
{

    /**
     * @var string
     */
    public static $tableName = 'customer';

    /**
     * Get all the customers from the database
     *
     * @param int $total
     *
     * @return Customer[]
     */
    public static function getCustomers($total = -1): array
    {
        $db = Database::connect();
        $tableName = self::$tableName;

        $query = $db->prepare("SELECT * FROM $tableName LIMIT :total");
        $query->bindValue(':total', $total, PDO::PARAM_INT);
        $query->execute();

        return $query->fetchAll(PDO::FETCH_CLASS, Customer::class);
    }
}