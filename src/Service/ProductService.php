<?php

namespace Minderest\Service;

use Minderest\Model\Product;
use PDO;

class ProductService
{

    /**
     * @var string
     */
    public static $tableName = 'product';

    /**
     * Insert a product
     *
     * @param      $name
     * @param      $customerId
     * @param null $relatedProductId
     *
     * @return bool|mixed
     */
    public static function insertProduct(
      $name,
      $customerId,
      $relatedProductId = null
    ) {
        $insertProduct = function (PDO $db) use ($name, $customerId) {
            $tableName = self::$tableName;
            $query = $db->prepare("INSERT INTO $tableName (name,customerId) " .
              "VALUES (:name,:customerId)");
            $query->execute([
              ':name' => $name,
              ':customerId' => $customerId,
            ]);
            return true;
        };
        return Database::transaction($insertProduct);
    }

    /**
     * Get customer's products
     *
     * @param int $customerId
     *
     * @return bool|Product[]
     */
    public static function getProductsByCustomer(int $customerId)
    {
        $db = Database::connect();
        $tableName = self::$tableName;

        $query = $db->prepare("SELECT * FROM $tableName WHERE customerId = :id");
        $query->execute([':id' => $customerId]);

        return $query->fetchAll();
    }

    /**
     * Create a relationship between 2 products
     *
     * @param int $fromProductId
     * @param int $toProductId
     *
     * @return bool|mixed
     */
    public static function relateProducts(int $fromProductId, int $toProductId)
    {
        $relateProducts = function (PDO $db) use (
          $fromProductId,
          $toProductId
        ) {
            $tableName = 'related_products';

            $query = $db->prepare("INSERT INTO $tableName (toId,fromId) VALUES (:toId, :fromId)");
            $query->execute([
              ':fromId' => $fromProductId,
              ':toId' => $toProductId,
            ]);
            return true;
        };

        return Database::transaction($relateProducts, true);
    }

    /**
     * Find unrelated products similar in text form different customers
     *
     * @param int    $customerId
     * @param string $text
     *
     * @return array
     */
    public static function findRelatedProduct(
      int $customerId,
      string $text
    ) {
        $db = Database::connect();

        $query = $db->prepare("SELECT product.*,customer.name AS customerName 
           FROM product
            LEFT JOIN customer 
                ON product.customerId = customer.id
            LEFT JOIN related_products
            ON product.id = related_products.toId OR product.id = related_products.fromId
            WHERE product.name LIKE :text
            AND customer.id != :customerId    
            AND (related_products.toId IS NULL OR related_products.fromId IS NULL)");

        $query->execute([
          ':customerId' => $customerId,
          ':text' => '%' . $text . '%',
        ]);

        return $query->fetchAll();
    }

}