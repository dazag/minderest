<?php


namespace Minderest\Controller;


use Minderest\Service\ProductService;

class ProductController
{

    const FILTER_ID = 'product_filter';

    const SEARCH_ACTION = 'search_action';

    const RELATE_PRODUCTS_ACTION = 'relate_products_action';

    const GET_PRODUCTS = 'get_products';

    /**
     * Process JSON requests
     */
    public function process()
    {
        $jsonParams = json_decode(file_get_contents("php://input"), true);
        if ($jsonParams[self::FILTER_ID]) {
            $this->filter($jsonParams);
        }
    }

    /**
     * Filter results by action
     * @param $data
     */
    private function filter($data)
    {
        $action = $data[self::FILTER_ID]['action'];
        $params = $data[self::FILTER_ID]['params'];
        $response = ['status' => 'success'];

        try {
            switch ($action) {
                case self::SEARCH_ACTION:
                    $text = $params['searchText'];
                    $customerId = $params['customerId'];
                    $response['data'] = ProductService::findRelatedProduct($customerId, $text);
                    break;
                case self::RELATE_PRODUCTS_ACTION:
                    $fromProductId = $params['fromProductId'];
                    $toProductId = $params['toProductId'];
                    $response['data'] = ProductService::relateProducts($fromProductId,
                      $toProductId);
                    break;
                case self::GET_PRODUCTS:
                    $customerId = $params['customerId'];
                    $response['data'] = ProductService::getProductsByCustomer($customerId);
                    break;
            }
        } catch (\Exception $exception) {
            $response['status'] = 'error';
            $response['message'] = $exception->getMessage();
        }

        if ($response['status'] == 'error') {
            header("HTTP/1.1 500");
        }
        header('Content-type: application/json');
        echo json_encode($response);
        die();
    }
}