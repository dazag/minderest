(function ($) {

    /**
     * The plugin namespace, ie for $('.selector').myPluginName(options)
     *
     * Also the id for storing the object state via $('.selector').data()
     */
    var PLUGIN_NS = 'searchProduct';
    var Plugin = function (target, options) {
        "use strict";
        this.$T = $(target);

        /** #### OPTIONS #### */
        this._options = $.extend(
            true,               // deep extend
            {
                endpoint: 'search.php',
                DEBUG: true,
            },
            options
        );

        /** #### PROPERTIES #### */
        this._currentCustomer = null;
        this._currentProduct = null;
        this._init(target, options);

        return this;
    };

    /** #### CONSTANTS #### */
    Plugin.FILTER_ID = 'product_filter';
    Plugin.SEARCH_ACTION = 'search_action';
    Plugin.RELATE_PRODUCTS_ACTION = 'relate_products_action';
    Plugin.GET_PRODUCTS = 'get_products';
    Plugin.CUSTOMER_SELECT_ID = 'select#customer';
    Plugin.PRODUCT_SELECT_ID = 'select#product';
    Plugin.SEARCH_TEXT_ID = 'input#search-text';

    /** #### INITIALISER #### */
    Plugin.prototype._init = function (target, settings) {
        var plugin = this;
        $productSelect = plugin.$T.find(Plugin.PRODUCT_SELECT_ID);
        $customerSelect = plugin.$T.find(Plugin.CUSTOMER_SELECT_ID);
        $searchTextInput = plugin.$T.find(Plugin.SEARCH_TEXT_ID);

        plugin._processCustomerSelect($customerSelect, $productSelect, $searchTextInput);
        plugin._processProductSelect($productSelect, $searchTextInput);
        plugin._processSearchText($searchTextInput);
    };

    Plugin.prototype.relateProduct = function (data) {
        var plugin = this;
        var productId = data[0].productId;
        var onSuccess = data[0].onSuccess;
        var params = {
            'product_filter': {
                'action': Plugin.RELATE_PRODUCTS_ACTION,
                'params': {
                    'fromProductId': productId,
                    'toProductId': plugin._currentProduct
                }
            }
        };

        plugin.dump("Request to relate product: " + productId + ", with: " + plugin._currentProduct);
        plugin._request(plugin._options.endpoint, params)
            .done(function () {
                onSuccess();
            });

        return plugin.$T;
    };

    Plugin.prototype._processCustomerSelect = function ($customerSelect, $productSelect, $searchTextInput) {
        var plugin = this;

        $customerSelect.on('change', function (event) {
            plugin._currentCustomer = event.target.value;

            var params = {
                'product_filter': {
                    'action': Plugin.GET_PRODUCTS,
                    'params': {
                        'customerId': plugin._currentCustomer
                    }
                }
            };
            plugin._request(plugin._options.endpoint, params)
                .done(function (data) {
                    // Clear select options
                    $productSelect
                        .find('option')
                        .remove()
                        .end()
                        .append("<option>-- Choose a product --</option>")
                        .trigger('change');

                    // Clear search text input
                    $searchTextInput.val('');

                    // Create product select options
                    $.each(data.data, function () {
                        $productSelect.append($("<option />").val(this.id).text(this.name));
                    });
                })
                .fail(function (jqXHR) {
                    var message = jqXHR.responseJSON.message;
                    plugin._showError(message);
                });
        });
    };

    Plugin.prototype._processProductSelect = function ($productSelect, $searchTextInput) {
        var plugin = this;

        $productSelect.on('change', function (event) {
            plugin._currentProduct = event.target.value;
            plugin.dump(plugin._currentProduct);
            if (!isNaN(plugin._currentProduct)) {
                $searchTextInput.attr('disabled', false);
            } else {
                $searchTextInput.attr('disabled', true);
            }
        });
    };

    Plugin.prototype._processSearchText = function ($searchTextInput) {
        var plugin = this;

        // add a delay for user key up
        function delay(callback, ms) {
            var timer = 0;
            return function () {
                var context = this, args = arguments;
                clearTimeout(timer);
                timer = setTimeout(function () {
                    callback.apply(context, args);
                }, ms || 0);
            };
        }

        // Ajax call on delay
        $searchTextInput.on('keyup', delay(function (event) {

            var searchText = event.target.value;
            if (!searchText || searchText.length < 3) {
                plugin.dump("Empty search text.");
                return false;
            }

            var params = {
                'product_filter': {
                    'action': Plugin.SEARCH_ACTION,
                    'params': {
                        'searchText': searchText,
                        'customerId': plugin._currentCustomer,
                    }
                }
            };
            plugin._request(plugin._options.endpoint, params)
                .done(function (data) {

                    plugin.$T.trigger(PLUGIN_NS + ':complete', data);
                })
                .fail(function (jqXHR) {
                    var message = jqXHR.responseJSON.message;
                    plugin._showError(message);
                });
        }, 500));
    };

    /**
     * @param url
     * @param params
     * @param type
     * @param dataType
     * @returns {XMLHttpRequest}
     */
    Plugin.prototype._request = function (url, params, type, dataType) {
        var data = {}, ajax = {}, contentType;

        dataType = dataType || 'json';
        type = type || 'POST';
        contentType = dataType === 'json' ? 'application/json; charset=UTF-8' : 'application/x-www-form-urlencoded; charset=UTF-8';

        if (dataType == 'json') {
            if (type == 'GET') {
                data = params;
            } else {
                data = JSON.stringify(params);
            }
        } else {
            data = params;

            ajax.processData = false;
            ajax.contentType = false;
        }

        ajax = $.extend(ajax, {
            type: type,
            url: url,
            data: data,
            // dataType: dataType,
            contentType: contentType,
            cache: false
        });

        return $.ajax(ajax);
    };

    Plugin.prototype._showError = function (message) {
        var plugin = this;
        plugin.dump("Error!");
        plugin.dump(message);
    };

    /**
     * EZ Logging/Warning (technically private but saving an '_' is worth it imo)
     */
    Plugin.prototype.dump = function () {
        if (!this._options.DEBUG || !console) return;
        for (var i in arguments) {
            console.log(PLUGIN_NS + ': ', arguments[i]);
        }
    };


    /*###################################################################################
     * JQUERY HOOK
     ###################################################################################*/

    /**
     * Generic jQuery plugin instantiation method call logic
     *
     * Method options are stored via jQuery's data() method in the relevant element(s)
     * Notice, myActionMethod mustn't start with an underscore (_) as this is used to
     * indicate private methods on the PLUGIN class.
     */
    $.fn[PLUGIN_NS] = function (methodOrOptions) {
        if (!$(this).length) {
            return $(this);
        }
        var instance = $(this).data(PLUGIN_NS);

        // CASE: action method (public method on PLUGIN class)
        if (instance
            && methodOrOptions.indexOf('_') != 0
            && instance[methodOrOptions]
            && typeof (instance[methodOrOptions]) == 'function') {

            return instance[methodOrOptions](Array.prototype.slice.call(arguments, 1));


            // CASE: argument is options object or empty = initialise
        } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {

            instance = new Plugin($(this), methodOrOptions);    // ok to overwrite if this is a re-init
            $(this).data(PLUGIN_NS, instance);
            return $(this);

            // CASE: method called before init
        } else if (!instance) {
            $.error('Plugin must be initialised before using method: ' + methodOrOptions);

            // CASE: invalid method
        } else if (methodOrOptions.indexOf('_') == 0) {
            $.error('Method ' + methodOrOptions + ' is private!');
        } else {
            $.error('Method ' + methodOrOptions + ' does not exist.');
        }
    };

})(jQuery);