CREATE DATABASE minderest CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE minderest;

# Customer structure
CREATE TABLE `customer`
(
    `id`   int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

# Product structure
CREATE TABLE `product`
(
    `id`               int(11) NOT NULL AUTO_INCREMENT,
    `name`             varchar(255) DEFAULT NULL,
    `customerId`       int(11)      DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `customer__fk` (`customerId`),
    CONSTRAINT `customer__fk` FOREIGN KEY (`customerId`) REFERENCES `customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
    ) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

# Customer structure
CREATE TABLE `related_products`
(
    `fromId` int(11) DEFAULT NULL,
    `toId`   int(11) DEFAULT NULL,
    KEY `fromProduct__fk` (`fromId`),
    KEY `toProduct__fk` (`toId`),
    CONSTRAINT `fromProduct__fk` FOREIGN KEY (`fromId`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `toProduct__fk` FOREIGN KEY (`toId`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

# Write 10 clients
LOCK TABLES `customer` WRITE;
INSERT INTO `customer` (`name`)
VALUES ('Primark'),
       ('Zara'),
       ('Oysho'),
       ('H&M'),
       ('Mango'),
       ('Bershka'),
       ('Stradivarius'),
       ('Armani'),
       ('Desigual'),
       ('Springfield');
UNLOCK TABLES;


