# Minderest Test App

### Instrucciones para Docker
Levantar contenedor: `docker-compose up -d`

Ejecutar consola de comandos de contenedor web: `docker-compose exec web /bin/bash`

URL de la web: `localhost`

### SQL output
El archivo output.sql contiene las instrucciones para generar todas las tablas y los 10 clientes

### Formulario AJAX para búsqueda y relación
Este formulario se encuentra en el archivo search.php.
Se ha creado un controllador "fake", a modo de ilustración para procesar las llamadas AJAX de
búsqueda, relación y obtención de productos.
Para el dinamismo del formulario con AJAX se ha optado por un plugin jquery searchProduct.jquery.js
Este se usa en conjunto con un trozo de script generado al final del documento search.php para listar los resultados.

### Formulario POST añadir productos
La información de esta vista se puede encontrar al completo en add.php

### Observaciones
He intentado plasmar muchos de los conocimientos de los que dispongo en este ejercicio, primando las tecnologías que se 
solicitan para ello. Podría estar aplicando mejoras sin fin pero creo que con el tiempo dedicado refleja bastante bien mi forma de trabajo.

Como es de esperar, un desarrollo de este tipo en un framework avanzado, reduciría la complejidad y tiempo dedicado al proyecto,
de todos modos estoy bastante contento con el resultado.

He incluído un servidor docker para las pruebas, eliminando así las dependencias de un sistema externo para la ejecución de las pruebas.
Este incluye una base de datos MariaDB y una imagen web basado en PHP 7.2 y NGINX.
