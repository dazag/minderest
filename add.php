<?php
include_once __DIR__ . '/src/Service/Database.php';
include_once __DIR__ . '/src/Model/Product.php';
include_once __DIR__ . '/src/Service/ProductService.php';
include_once __DIR__ . '/src/Model/Customer.php';
include_once __DIR__ . '/src/Service/CustomerService.php';

$result = false;
$productName = $_POST['product_name'] ?? '';
$customerId = $_POST['customer'] ?? null;

if (!empty($productName) && !is_null($customerId)) {
    $result = \Minderest\Service\ProductService::insertProduct($productName,
      $customerId);
}

// Load form data
$customers = \Minderest\Service\CustomerService::getCustomers();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Minderest Test App</title>

    <!-- Font-awesome | Bootstrap doesn't provide icons anymore-->
    <script src="https://kit.fontawesome.com/925f719f43.js"></script>

    <!--    Bootstrap CSS-->
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>

    <link rel="stylesheet" href="assets/app.css">
</head>
<body>
<header>
    <div class="bg-dark collapse" id="navbarHeader">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-md-7 py-4">
                    <h4 class="text-white">About</h4>
                    <p class="text-muted">I am a PHP Full Stack Senior Developer
                        called David Zamora, here you can see the test.</p>
                </div>
                <div class="col-sm-4 offset-md-1 py-4">
                    <h4 class="text-white">Contact</h4>
                    <ul class="list-unstyled">
                        <li><a href="mailto:david@zamoragutierrez.com"
                               class="text-white">Email me</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="navbar navbar-dark bg-dark shadow-sm">
        <div class="container d-flex justify-content-between">
            <a href="/" class="navbar-brand d-flex align-items-center">
                <i class="fas fa-brain"></i>&nbsp;
                <strong>Minderest test app</strong>
            </a>
            <button class="navbar-toggler collapsed" type="button"
                    data-toggle="collapse" data-target="#navbarHeader"
                    aria-controls="navbarHeader" aria-expanded="false"
                    aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
    </div>
</header>
<main role="main">

    <div class="container">
        <section class="jumbotron text-center">
            <div class="container">
                <h1 class="jumbotron-heading">Minderest Search Post Form</h1>
                <p class="lead text-muted">Below you can find any actions you
                    need
                    to accomplish this test, please <strong>choose one.</strong>
                </p>
                <p>
                    <a href="/" class="btn btn-primary my-2">Go Home</a>
                    <a href="search.php" class="btn btn-secondary my-2">Search
                        Products</a>
                </p>
            </div>
        </section>

        <?php
        if ($result) {
            ?>
            <div class="alert alert-success" role="alert">
                Product <?= $productName ?> has been added to the database.
            </div>
            <?php
        }
        ?>

        <form method="POST">
            <div class="form-group">
                <label for="product_name">Product Name</label>
                <input type="text" class="form-control"
                       id="product_name"
                       name="product_name"
                       placeholder="Name">
            </div>
            <div class="form-group">
                <label for="customer">Select a Client</label>
                <select class="form-control" id="customer" name="customer">
                    <option>-- Choose a customer --</option>
                    <?php
                    foreach ($customers as $customer) {
                        ?>
                        <option value="<?= $customer->getId() ?>">
                            <?= $customer->getName() ?>
                        </option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</main>

<footer class="text-muted">
    <div class="container">
        <p>Test Example project</p>
        <p>New to Bootstrap? <a href="https://getbootstrap.com/">Visit the
                homepage</a> or read our <a
                    href="/docs/4.3/getting-started/introduction/">getting
                started guide</a>.</p>
    </div>
</footer>

<!--load scripts in the end to improve web loading-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>
</html>